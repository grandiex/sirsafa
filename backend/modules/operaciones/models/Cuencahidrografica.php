<?php

namespace backend\modules\operaciones\models;

use Yii;

/**
 * This is the model class for table "cuenca_hidrografica".
 *
 * @property int $idcuenca_hidrografica Se Almacena el dominio de la persona juridica - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 * @property int $region_hidrografica_idregion_hidrografica
 *
 * @property Aprovechamiento[] $aprovechamientos
 * @property RegionHidrografica $regionHidrograficaIdregionHidrografica
 */
class Cuencahidrografica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuenca_hidrografica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id', 'region_hidrografica_idregion_hidrografica'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'region_hidrografica_idregion_hidrografica'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
            [['region_hidrografica_idregion_hidrografica'], 'exist', 'skipOnError' => true, 'targetClass' => Regionhidrografica::className(), 'targetAttribute' => ['region_hidrografica_idregion_hidrografica' => 'idregion_hidrografica']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcuenca_hidrografica' => 'Idcuenca Hidrografica',
            'descripcion' => 'Descripcion',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'region_hidrografica_idregion_hidrografica' => 'Region Hidrografica',
        ];
    }

    /**
     * Gets query for [[Aprovechamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientos()
    {
        return $this->hasMany(Aprovechamiento::className(), ['cuenca_hidrografica_idcuenca_hidrografica' => 'idcuenca_hidrografica']);
    }

    /**
     * Gets query for [[RegionHidrograficaIdregionHidrografica]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegionhidrografica()
    {
        return $this->hasOne(Regionhidrografica::className(), ['idregion_hidrografica' => 'region_hidrografica_idregion_hidrografica']);
    }
}
