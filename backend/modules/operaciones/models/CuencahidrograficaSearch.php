<?php

namespace backend\modules\operaciones\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\operaciones\models\Cuencahidrografica;

/**
 * CuencahidrograficaSearch represents the model behind the search form of `backend\modules\operaciones\models\Cuencahidrografica`.
 */
class CuencahidrograficaSearch extends Cuencahidrografica
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcuenca_hidrografica', 'user_id', 'region_hidrografica_idregion_hidrografica'], 'integer'],
            [['descripcion', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cuencahidrografica::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcuenca_hidrografica' => $this->idcuenca_hidrografica,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'region_hidrografica_idregion_hidrografica' => $this->region_hidrografica_idregion_hidrografica,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
