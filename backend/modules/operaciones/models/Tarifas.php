<?php

namespace backend\modules\operaciones\models;

use Yii;

/**
 * This is the model class for table "tarifas".
 *
 * @property int $idtarifas Tabla para almacenar nombres de los posibles sistemas que existan en el ecosistema - Pertenece al módulo de seguridad
 * @property string $justificacion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 * @property int $fuente_agua_idfuente_agua
 * @property int $usos_idusos
 * @property int $tipo_usos_idtipo_usos
 * @property string $tipo_persona
 * @property float $unidad_cuenta
 * @property string $estatus
 *
 * @property AprovechamientoUsos[] $aprovechamientoUsos
 */
class Tarifas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarifas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['justificacion', 'created_at', 'updated_at', 'user_id', 'fuente_agua_idfuente_agua', 'usos_idusos', 'tipo_usos_idtipo_usos', 'tipo_persona', 'unidad_cuenta', 'estatus'], 'required'],
            [['justificacion'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'fuente_agua_idfuente_agua', 'usos_idusos', 'tipo_usos_idtipo_usos'], 'integer'],
            [['unidad_cuenta'], 'number'],
            [['tipo_persona', 'estatus'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtarifas' => 'Tarifas',
            'justificacion' => 'Justificacion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
            'fuente_agua_idfuente_agua' => 'Fuente Agua',
            'usos_idusos' => 'Usos',
            'tipo_usos_idtipo_usos' => 'Tipo Usos',
            'tipo_persona' => 'Tipo Persona',
            'unidad_cuenta' => 'Unidad Cuenta',
            'estatus' => 'Estatus',
        ];
    }

    /**
     * Gets query for [[AprovechamientoUsos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientoUsos()
    {
        return $this->hasMany(AprovechamientoUsos::className(), ['tarifas_idtarifas' => 'idtarifas']);
    }
}
