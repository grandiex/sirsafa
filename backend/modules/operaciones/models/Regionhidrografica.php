<?php

namespace backend\modules\operaciones\models;

use Yii;

/**
 * This is the model class for table "region_hidrografica".
 *
 * @property int $idregion_hidrografica Se Almacenan las regiones hidrograficas -  Pertenece al módulo de Administración del sistema
 * @property string $identificacion
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Aprovechamiento[] $aprovechamientos
 * @property CuencaHidrografica[] $cuencaHidrograficas
 */
class Regionhidrografica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'region_hidrografica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['identificacion', 'descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['identificacion'], 'string', 'max' => 10],
            [['descripcion'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idregion_hidrografica' => 'Idregion Hidrografica',
            'identificacion' => 'Identificacion',
            'descripcion' => 'Descripcion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Aprovechamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientos()
    {
        return $this->hasMany(Aprovechamiento::className(), ['region_hidrografica_idregion_hidrografica' => 'idregion_hidrografica']);
    }

    /**
     * Gets query for [[CuencaHidrograficas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuencaHidrograficas()
    {
        return $this->hasMany(CuencaHidrografica::className(), ['region_hidrografica_idregion_hidrografica' => 'idregion_hidrografica']);
    }
}
