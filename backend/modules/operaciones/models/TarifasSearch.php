<?php

namespace backend\modules\operaciones\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\operaciones\models\Tarifas;

/**
 * TarifasSearch represents the model behind the search form of `backend\modules\operaciones\models\Tarifas`.
 */
class TarifasSearch extends Tarifas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtarifas', 'user_id', 'fuente_agua_idfuente_agua', 'usos_idusos', 'tipo_usos_idtipo_usos'], 'integer'],
            [['justificacion', 'created_at', 'updated_at', 'tipo_persona', 'estatus'], 'safe'],
            [['unidad_cuenta'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tarifas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idtarifas' => $this->idtarifas,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'fuente_agua_idfuente_agua' => $this->fuente_agua_idfuente_agua,
            'usos_idusos' => $this->usos_idusos,
            'tipo_usos_idtipo_usos' => $this->tipo_usos_idtipo_usos,
            'unidad_cuenta' => $this->unidad_cuenta,
        ]);

        $query->andFilterWhere(['like', 'justificacion', $this->justificacion])
            ->andFilterWhere(['like', 'tipo_persona', $this->tipo_persona])
            ->andFilterWhere(['like', 'estatus', $this->estatus]);

        return $dataProvider;
    }
}
