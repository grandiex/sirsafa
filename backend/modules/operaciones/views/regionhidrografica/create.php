<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Regionhidrografica */

$this->title = Yii::t('app', 'Nueva Region Hidrográfica');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Region Hidrográfica'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regionhidrografica-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
