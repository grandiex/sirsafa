<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Regionhidrografica */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="regionhidrografica-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

            <?= $form->field($model, 'identificacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
