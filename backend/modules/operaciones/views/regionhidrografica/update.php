<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Regionhidrografica */

$this->title = Yii::t('app', 'Actualizar Región Hidrográfica: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Región Hidrográfica'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idregion_hidrografica, 'url' => ['view', 'id' => $model->idregion_hidrografica]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="regionhidrografica-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
