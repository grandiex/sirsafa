<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Cuencahidrografica */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuencahidrografica-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

            <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'region_hidrografica_idregion_hidrografica')->textInput() ?>

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
