<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Cuencahidrografica */

$this->title = Yii::t('app', 'Nueva Cuenca Hidrográfica');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuenca Hidrográfica'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuencahidrografica-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
