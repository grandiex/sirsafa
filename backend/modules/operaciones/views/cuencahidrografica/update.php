<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Cuencahidrografica */

$this->title = Yii::t('app', 'Actualizar Cuenca Hidrográfica: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuencahidrograficas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcuenca_hidrografica, 'url' => ['view', 'id' => $model->idcuenca_hidrografica]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="cuencahidrografica-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
