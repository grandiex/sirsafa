<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Tarifas */

$this->title = Yii::t('app', 'Nueva Tarifas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifas-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
