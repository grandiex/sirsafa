<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Tarifas */

$this->title = Yii::t('app', 'Actualizar Tarifas: {name}', [
    'name' => $model->idtarifas,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtarifas, 'url' => ['view', 'id' => $model->idtarifas]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="tarifas-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
