<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\operaciones\models\Tarifas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifas-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

            <?= $form->field($model, 'justificacion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fuente_agua_idfuente_agua')->textInput() ?>

    <?= $form->field($model, 'usos_idusos')->textInput() ?>

    <?= $form->field($model, 'tipo_usos_idtipo_usos')->textInput() ?>

    <?= $form->field($model, 'tipo_persona')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unidad_cuenta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estatus')->textInput(['maxlength' => true]) ?>

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
