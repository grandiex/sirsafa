<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\select2\Select2; 
use yii\helpers\ArrayHelper;

use backend\modules\seguridad\models\Modulo;
use backend\modules\seguridad\models\Programa;
use backend\modules\seguridad\models\Perfiles;
/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Perfilacceso */
/* @var $form yii\widgets\ActiveForm */

if (!$model->isNewRecord) {
    $programadata= ArrayHelper::map(Programa::find()->orderBy('descripcion ASC')->all(), 'idmodulo','descripcion');
} else {
     $programadata="";
}



?>

<div class="perfilacceso-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'perfiles_idperfiles')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Perfiles::find()->orderBy('descripcion ASC')->all(), 'idperfiles','descripcion', 'denominacionperfil'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',

                        
                    ],
            //],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>

    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'modulo_idmodulo')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Modulo::find()->orderBy('descripcion ASC')->all(), 'idmodulo','descripcion', 'moduledescription'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...',

            'onchange'  => '
                            $.post("../programa/listprograma?id=' . '" + $(this).val(), function(data){
                                $("select#perfilacceso-programa_idmodulo").html(data);
                            })
                        ',

            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <?= $form->field($model, 'programa_idmodulo')->widget(Select2::classname(), [
            'data' => $programadata,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    

    <div class="col-md-3 col-sm-3">
        
         <?= $form->field($model, 'nuevo')->widget(SwitchInput::classname(), []) ?>
    </div>
    <div class="col-md-3 col-sm-3">
        
         <?= $form->field($model, 'editar')->widget(SwitchInput::classname(), []) ?>
    </div>
    <div class="col-md-3 col-sm-3">
        
         <?= $form->field($model, 'examinar')->widget(SwitchInput::classname(), []) ?>
    </div>
    <div class="col-md-3 col-sm-3">
        
         <?= $form->field($model, 'eliminar')->widget(SwitchInput::classname(), []) ?>
    </div>

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
