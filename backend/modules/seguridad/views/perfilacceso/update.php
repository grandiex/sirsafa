<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Perfilacceso */

$this->title = Yii::t('app', 'Actualizar Acceso: {name}', [
    'name' => $model->perfiles->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accesos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idperfil_modulo, 'url' => ['view', 'id' => $model->idperfil_modulo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="perfilacceso-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
