<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Perfilacceso */

$this->title = Yii::t('app', 'Nuevo Acceso a Perfiles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accesos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perfilacceso-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
