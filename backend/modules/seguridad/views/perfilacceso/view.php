<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Perfilacceso */

$this->title = 'Acceso del perfil: '.$model->perfiles->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perfilaccesos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="perfilacceso-view">

    <h1  class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idperfil_modulo, 'idprogram'=>$idprogram,], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idperfil_modulo, 'idprogram'=>$idprogram,], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idperfil_modulo',
            //'modulo_idmodulo',
            //'programa_idmodulo',
            [

                'attribute' => 'perfiles_idperfiles', 
                'value' => $model->perfiles->descripcion, 
            ],
            [

                'attribute' => 'modulo_idmodulo', 
                'value' => $model->modulo->descripcion, 
            ],
            [

                'attribute' => 'programa_idmodulo', 
                'value' => $model->programa->descripcion, 
            ],
            //'perfiles_idperfiles',
            [
                'attribute' => 'nuevo',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->nuevo==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'editar',
            [
                'attribute' => 'editar',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->editar==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'examinar',
            [
                'attribute' => 'examinar',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->examinar==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'eliminar',
            [
                'attribute' => 'eliminar',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->eliminar==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            'created_at',
            'updated_at',
            //'user_id',
        ],
    ]) ?>

</div>
