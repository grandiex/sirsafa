<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\PerfilaccesoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="perfilacceso-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idperfil_modulo') ?>

    <?= $form->field($model, 'modulo_idmodulo') ?>

    <?= $form->field($model, 'programa_idmodulo') ?>

    <?= $form->field($model, 'perfiles_idperfiles') ?>

    <?= $form->field($model, 'nuevo') ?>

    <?php // echo $form->field($model, 'editar') ?>

    <?php // echo $form->field($model, 'examinar') ?>

    <?php // echo $form->field($model, 'eliminar') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
