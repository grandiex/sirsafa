<?php


use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use kartik\growl\Growl;
use yii\helpers\ArrayHelper;

use backend\modules\seguridad\models\Modulo;
use backend\modules\seguridad\models\Programa;
use backend\modules\seguridad\models\Perfiles;

use backend\modules\seguridad\models\Perfilacceso;

$idprogram=$_GET["idprogram"];

$programa = Perfilacceso::find()->where("idperfil_modulo=$idprogram")->one();
$template="";


if ($programa->examinar==1) {
    $template.= '{view} ' ;
}
if ($programa->editar==1) {
    $template.= '{update} ' ;
}
if ($programa->eliminar==1) {
    $template.= '{delete} ' ;
}


if ($programa->nuevo==1) {
    $newregister= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'idprogram'=>$idprogram,], ['class' => 'btn btn-success', 'title' => 'Nuevo Registro']) ;
}else{
    $newregister="";
}

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\seguridad\models\PerfilaccesoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Accesos');
$this->params['breadcrumbs'][] = $this->title;

if ( Yii::$app->session->getFlash('success') ) {
    $type = Growl::TYPE_SUCCESS;
    $title = 'Guardar Registro';
    $icon = 'glyphicon glyphicon-ok-sign';
    $body = Yii::$app->session->getFlash('success');
} elseif ( Yii::$app->session->getFlash('error') ) {
    $type = Growl::TYPE_DANGER;
    $title = 'Error al Guardar Registro';
    $icon = 'glyphicon glyphicon-remove-sign';
    $body = Yii::$app->session->getFlash('error');
} elseif ( Yii::$app->session->getFlash('warning') ) {
    $type = Growl::TYPE_WARNING;
    $title = 'Registro Eliminado';
    $icon = 'glyphicon glyphicon-exclamation-sign';
    $body = Yii::$app->session->getFlash('warning');
}

$flashMessages = Yii::$app->session->getAllFlashes();
if ($flashMessages) {
        echo Growl::widget([
            'type' => $type,
            'title' => $title,
            'icon' => $icon,
            'body' => $body,
            'showSeparator' => true,
            'delay' => 0,
            'pluginOptions' => [
                'showProgressbar' => true,
                'placement' => [
                    'from' => 'top',
                    'align' => 'center',
                ],
                'timer' => 1000,
            ]
        ]);
}
?>
<div class="perfilacceso-index">


    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'idmodulo',
            //'sistemas_idsistemas',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'perfiles_idperfiles', 
                'value' => 'perfiles.descripcion', 
                //'label'=>'Estatus',
                //'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Perfiles::find()->orderBy('descripcion ASC')->all(), 'idperfiles','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'modulo_idmodulo', 
                'value' => 'modulo.descripcion', 
                //'label'=>'Estatus',
                //'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Modulo::find()->orderBy('descripcion ASC')->all(), 'idmodulo','descripcion', 'moduledescription'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'programa_idmodulo', 
                'value' => 'programa.descripcion', 
                //'label'=>'Estatus',
                //'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Programa::find()->orderBy('descripcion ASC')->all(), 'idmodulo','descripcion','programadescription'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            //'idperfil_modulo',
            //'modulo_idmodulo',
            //'programa_idmodulo',
            
            //'nuevo',
            [
                'attribute' => 'nuevo',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->nuevo==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'editar',
            [ 
                'attribute' => 'editar',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->editar==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'examinar',
            [
                'attribute' => 'examinar',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->examinar==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'eliminar',
            [
                'attribute' => 'eliminar',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->eliminar==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            //'created_at',
            //'updated_at',
            //'user_id',
            
            
        ['class' => 'kartik\grid\ActionColumn', 

            'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => $template,
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },

                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Eliminar'),
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                    ]);
                },


                ],

                'urlCreator' => function ($action, $model, $key, $index) use ($idprogram){
                
                    if ($action === 'view') {
                        $url =['/seguridad/perfilacceso/view?idprogram='.$idprogram.'&id='.$model->idperfil_modulo];
                        return $url;
                    }
                    if ($action === 'update') {
                        $url =['/seguridad/perfilacceso/update?idprogram='.$idprogram.'&id='.$model->idperfil_modulo];
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url =['/seguridad/perfilacceso/delete?idprogram='.$idprogram.'&id='.$model->idperfil_modulo];
                        return $url;
                    }
                      

                }

        ],
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                $newregister  . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true, 
        
        'panel' => [
            'heading'=>'<h1 class="h2-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h2>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>


</div>
