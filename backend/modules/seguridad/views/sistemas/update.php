<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Sistemas */

$this->title = Yii::t('app', 'Actualizar Sistema: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sistemas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsistemas, 'url' => ['view', 'id' => $model->idsistemas]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="sistemas-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
