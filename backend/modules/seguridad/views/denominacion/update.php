<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Denominacion */

$this->title = Yii::t('app', 'Actualizar Denominación: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Denominaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddenominacion, 'url' => ['view', 'id' => $model->iddenominacion]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="denominacion-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
