<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Usuariointerno */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuariointerno-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

           
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-sm-6">
            
            <?= $form->field($model, 'nacionalidad')->widget(Select2::classname(), [
            'data' =>['V'=>'VENEZOLANO', 'E'=>'EXTRANGERO'],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'documento_identidad')->textInput() ?>

        </div>
        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'cargo')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            
            <?= $form->field($model, 'telefono_movil')->widget(MaskedInput::classname(),[
                'name'=>'documento_identidad',
                'mask'=>'(9999) 999.99.99',
            ]) ?>
        </div>
        <div class="col-md-4 col-sm-4">
            
            <?= $form->field($model, 'telefono_oficina')->widget(MaskedInput::classname(),[
                'name'=>'documento_identidad',
                'mask'=>'(9999) 999.99.99',
                
            ]) ?>
        </div>
    

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?php 
            if (!$model->isNewRecord) {
                echo Html::a('Cancelar', ['index','idprogram'=>$idprogram], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']);
            } else {
                echo Html::a('Cancelar', ['/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']);
            }
            ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?> 

</div>
