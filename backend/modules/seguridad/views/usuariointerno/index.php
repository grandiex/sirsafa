<?php


use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use kartik\growl\Growl;
use yii\helpers\ArrayHelper;
 
use backend\modules\seguridad\models\Perfiles;
use backend\modules\seguridad\models\Perfilacceso;

$idprogram=$_GET["idprogram"];

$programa = Perfilacceso::find()->where("idperfil_modulo=$idprogram")->one();
$template="";


if ($programa->examinar==1) {
    $template.= '{view} ' ;
}
if ($programa->editar==1) {
    $template.= '{update} ' ;
}
if ($programa->eliminar==1) {
    $template.= '{delete} ' ;
}
if ($programa->editar==1) {
    $template.= '{perfil} ' ;
}

if ($programa->nuevo==1) {
    $newregister= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => 'Nuevo Registro']) ;
}else{
    $newregister="";
}

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\seguridad\models\UsuariointernoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usuarios Internos');
$this->params['breadcrumbs'][] = $this->title;

if ( Yii::$app->session->getFlash('success') ) {
    $type = Growl::TYPE_SUCCESS;
    $title = 'Guardar Registro';
    $icon = 'glyphicon glyphicon-ok-sign';
    $body = Yii::$app->session->getFlash('success');
} elseif ( Yii::$app->session->getFlash('error') ) {
    $type = Growl::TYPE_DANGER;
    $title = 'Error al Guardar Registro';
    $icon = 'glyphicon glyphicon-remove-sign';
    $body = Yii::$app->session->getFlash('error');
} elseif ( Yii::$app->session->getFlash('warning') ) {
    $type = Growl::TYPE_WARNING;
    $title = 'Registro Eliminado';
    $icon = 'glyphicon glyphicon-exclamation-sign';
    $body = Yii::$app->session->getFlash('warning');
}

$flashMessages = Yii::$app->session->getAllFlashes();
if ($flashMessages) {
        echo Growl::widget([
            'type' => $type,
            'title' => $title,
            'icon' => $icon,
            'body' => $body,
            'showSeparator' => true,
            'delay' => 0,
            'pluginOptions' => [
                'showProgressbar' => true,
                'placement' => [
                    'from' => 'top',
                    'align' => 'center',
                ],
                'timer' => 1000,
            ]
        ]);
}
?>
<div class="usuariointerno-index">


    <?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'idmodulo',
            //'sistemas_idsistemas',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'perfiles_idperfiles', 
                'value' => 'perfiles.descripcion', 
                //'label'=>'Estatus',
                //'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Perfiles::find()->orderBy('descripcion ASC')->all(), 'idperfiles','descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            //'idusuario_interno',
            //'perfiles_idperfiles',
            'nombres',
            'apellidos',
            'nacionalidad',
            'documento_identidad',
            'cargo',
            'telefono_movil',
            'telefono_oficina',
            //'created_at',
            //'updated_at',
            //'user_id',
            
            
    ['class' => 'kartik\grid\ActionColumn', 

        'headerOptions' => ['style' => 'color:#337ab7'],
                            'template' => $template,
                            'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                            'title' => Yii::t('app', 'Ver'),
                                ]);
                            },
             
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                            'title' => Yii::t('app', 'Actualizar'),
                                ]);
                            },

                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => Yii::t('app', 'Eliminar'),
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
                                ]);
                            },

                            'perfil' => function ($url, $model) {
                                return Html::a('<span class="text-gold glyphicon glyphicon-user"></span>', $url, [
                                            'title' => Yii::t('app', 'Asignar Perfil'),
                                ]);
                            },
                            
             
                            ],

                            'urlCreator' => function ($action, $model, $key, $index) use ($idprogram){
                            /*if ($action === 'update') {
                                $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                                return $url;
                            }*/
                            if ($action === 'view') {
                                $url =['/seguridad/usuariointerno/view?idprogram='.$idprogram.'&id='.$model->idusuario_interno];
                                return $url;
                            }
                            if ($action === 'update') {
                                $url =['/seguridad/usuariointerno/update?idprogram='.$idprogram.'&id='.$model->idusuario_interno];
                                return $url;
                            }
                            if ($action === 'delete') {
                                $url =['/seguridad/usuariointerno/delete?idprogram='.$idprogram.'&id='.$model->idusuario_interno];
                                return $url;
                            }

                            if ($action === 'perfil') {
                                $url =['/seguridad/usuariointerno/asignaperfil?idprogram='.$idprogram.'&id='.$model->idusuario_interno];
                                return $url;
                            }
                              
             
                            }

    

    //'urlCreator' => function($action, $model, $key, $index) { return '#'; },

    ],
            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                $newregister . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true, 
        
        'panel' => [
            'heading'=>'<h1 class="h2-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h2>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

//echo $template ;

    ?>


</div>
