<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Usuariointerno */

$this->title = Yii::t('app', 'Datos Personales del Usuario: {name}', [
    'name' => $model->nombres . ' '. $model->apellidos,]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usuariointernos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuariointerno-view">

    <h1  class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idusuario_interno], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idusuario_interno], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idusuario_interno',
            //'perfiles_idperfiles',
            'nombres',
            'apellidos',
            'nacionalidad',
            'documento_identidad',
            'cargo',
            'telefono_movil',
            'telefono_oficina',
            //'created_at',
            //'updated_at',
            //'user_id',
        ],
    ]) ?>

</div>
