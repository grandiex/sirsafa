<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Usuariointerno */

$this->title = Yii::t('app', 'Asignación del Perfil al Usuario: {name}', [
    'name' => $model->nombres . ' '. $model->apellidos,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perfil Ususario'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idusuario_interno, 'url' => ['view', 'id' => $model->idusuario_interno]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="usuariointerno-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formasignaperfil', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
