<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Usuariointerno */

$this->title = Yii::t('app', 'Actualización de Datos Personales');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datos Personales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuariointerno-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
