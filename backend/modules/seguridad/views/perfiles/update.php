<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Perfiles */

$this->title = Yii::t('app', 'Actualizar Perfil: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perfiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idperfiles, 'url' => ['view', 'id' => $model->idperfiles]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="perfiles-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
