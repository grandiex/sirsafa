<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\seguridad\models\Modulo */

$this->title = Yii::t('app', 'Actualizar Modulo: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modulos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idmodulo, 'url' => ['view', 'id' => $model->idmodulo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="modulo-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
