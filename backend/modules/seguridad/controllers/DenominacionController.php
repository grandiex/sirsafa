<?php

namespace backend\modules\seguridad\controllers;

use Yii;
use backend\modules\seguridad\models\Denominacion;
use backend\modules\seguridad\models\DenominacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DenominacionController implements the CRUD actions for Denominacion model.
 */
class DenominacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Denominacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(['/user/security/login']);
        }else {
            $searchModel = new DenominacionSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Denominacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $idprogram)
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(['/user/security/login']);
        }else {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'idprogram'=>$idprogram,
            ]);
        }
    }

    /**
     * Creates a new Denominacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idprogram)
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = new Denominacion();
            $model->created_at=date("Y-m-d");
            $model->updated_at=date("Y-m-d");
            $model->user_id=Yii::$app->user->identity->getId();

             if ($model->load(Yii::$app->request->post())) {

                 if ( $model->save() ) {
                            Yii::$app->session->setFlash('success','El Registro de la Denominación '.$model->descripcion.' se realizó correctamente');
                            return $this->redirect(['index', 'idprogram'=>$idprogram,]);
                    } else {

                        $errores = "";

                        foreach ( $model->getErrors() as $key => $value ) {
                            foreach ( $value as $row => $field ) {
                                $errores .= $field . "<br>";
                            }
                        }

                        Yii::$app->session->setFlash('error',$errores);
                        return $this->redirect(['index', 'idprogram'=>$idprogram,]);
                    }
            }else{

                return $this->render('create', [
                    'model' => $model,
                    'idprogram'=>$idprogram,
                ]);
            }
        }
    }

    /**
     * Updates an existing Denominacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $idprogram)
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(['/user/security/login']);
        }else {

            $model = $this->findModel($id);
            $model->updated_at=date("Y-m-d");
            $model->user_id=Yii::$app->user->identity->getId();

            if ($model->load(Yii::$app->request->post())) {

                if ( $model->save() ) {
                        Yii::$app->session->setFlash('success','La Actualización de la Denominación '.$model->descripcion.' se realizó correctamente');
                        return $this->redirect(['index', 'idprogram'=>$idprogram,]);
                } else {

                    $errores = "";

                    foreach ( $model->getErrors() as $key => $value ) {
                        foreach ( $value as $row => $field ) {
                            $errores .= $field . "<br>";
                        }
                    }

                    Yii::$app->session->setFlash('error',$errores);
                    return $this->redirect(['index', 'idprogram'=>$idprogram,]);
                }
            }else{

                return $this->render('update', [
                    'model' => $model,
                    'idprogram'=>$idprogram,
                ]);
            }

        }
    }

    /**
     * Deletes an existing Denominacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $idprogram)
    {
        
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(['/user/security/login']);
        }else {

            
            $model = $this->findModel($id);

            if ( $model->delete() ) {
                        Yii::$app->session->setFlash('warning','Se Eliminó la Denominación '.$model->descripcion);
                        return $this->redirect(['index', 'idprogram'=>$idprogram,]);
                } else {

                    $errores = "";

                    foreach ( $model->getErrors() as $key => $value ) {
                        foreach ( $value as $row => $field ) {
                            $errores .= $field . "<br>";
                        }
                    }

                    Yii::$app->session->setFlash('error',$errores);
                    return $this->redirect(['index', 'idprogram'=>$idprogram,]);
                }

            return $this->redirect(['index', 'idprogram'=>$idprogram,]);
        }
    }

    /**
     * Finds the Denominacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Denominacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Denominacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
