<?php

namespace backend\modules\seguridad\models;

use Yii;

/**
 * This is the model class for table "programa".
 *
 * @property int $idmodulo Tabla para almacenar nombres de los programas y rutas que existan en el módulo seleccionado - Pertenece al módulo de seguridad
 * @property string $descripcion
 * @property string $imagen
 * @property string $ruta
 * @property string $información
 * @property int $modulo_idmodulo
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property PerfilAcceso[] $perfilAccesos
 * @property Modulo $moduloIdmodulo
 */
class Programa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'programa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'imagen', 'ruta', 'información', 'modulo_idmodulo', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['modulo_idmodulo', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['descripcion', 'imagen'], 'string', 'max' => 45],
            [['ruta', 'información'], 'string', 'max' => 200],
            [['descripcion'], 'unique'],
            [['ruta'], 'unique'],
            [['modulo_idmodulo'], 'exist', 'skipOnError' => true, 'targetClass' => Modulo::className(), 'targetAttribute' => ['modulo_idmodulo' => 'idmodulo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmodulo' => 'Idmodulo',
            'descripcion' => 'Descripción',
            'imagen' => 'Imagen',
            'ruta' => 'Ruta',
            'información' => 'Información',
            'modulo_idmodulo' => 'Módulo',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[PerfilAccesos]]. 
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfilAccesos()
    {
        return $this->hasMany(PerfilAcceso::className(), ['programa_idmodulo' => 'idmodulo']);
    }

    /**
     * Gets query for [[ModuloIdmodulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulo::className(), ['idmodulo' => 'modulo_idmodulo']);
    }

    

    /**
     * Gets query for [[ModuloIdmodulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProgramadescription()
    {
        return $this->modulo->descripcion;
        
    }

    
}
