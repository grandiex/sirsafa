<?php

namespace backend\modules\seguridad\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\seguridad\models\Usuariointerno;

/**
 * UsuariointernoSearch represents the model behind the search form of `backend\modules\seguridad\models\Usuariointerno`.
 */
class UsuariointernoSearch extends Usuariointerno
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idusuario_interno', 'perfiles_idperfiles', 'documento_identidad', 'user_id'], 'integer'],
            [['nombres', 'apellidos', 'nacionalidad', 'cargo', 'telefono_movil', 'telefono_oficina', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuariointerno::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idusuario_interno' => $this->idusuario_interno,
            'perfiles_idperfiles' => $this->perfiles_idperfiles,
            'documento_identidad' => $this->documento_identidad,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad])
            ->andFilterWhere(['like', 'cargo', $this->cargo])
            ->andFilterWhere(['like', 'telefono_movil', $this->telefono_movil])
            ->andFilterWhere(['like', 'telefono_oficina', $this->telefono_oficina]);

        return $dataProvider;
    }
}
