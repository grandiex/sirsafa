<?php

namespace backend\modules\seguridad\models;

use Yii;

/**
 * This is the model class for table "modulo".
 *
 * @property int $idmodulo Tabla para almacenar nombres de los posibles módulos que existan en el sistema seleccionado - Pertenece al módulo de seguridad
 * @property int $sistemas_idsistemas
 * @property string $descripcion
 * @property string $imagen
 * @property string $ruta
 * @property string $información
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Sistemas $sistemasIdsistemas
 * @property PerfilAcceso[] $perfilAccesos
 * @property Programa[] $programas
 */
class Modulo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modulo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sistemas_idsistemas', 'descripcion', 'imagen', 'ruta', 'información', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['sistemas_idsistemas', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['descripcion', 'imagen'], 'string', 'max' => 45],
            [['ruta', 'información'], 'string', 'max' => 200],
            [['descripcion'], 'unique'],
            [['ruta'], 'unique'],
            [['sistemas_idsistemas'], 'exist', 'skipOnError' => true, 'targetClass' => Sistemas::className(), 'targetAttribute' => ['sistemas_idsistemas' => 'idsistemas']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmodulo' => 'Idmodulo',
            'sistemas_idsistemas' => 'Sistema',
            'descripcion' => 'Descripción',
            'imagen' => 'Imagen',
            'ruta' => 'Ruta',
            'información' => 'Información',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[SistemasIdsistemas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSistemas()
    {
        return $this->hasOne(Sistemas::className(), ['idsistemas' => 'sistemas_idsistemas']);
    }

    /**
     * Gets query for [[PerfilAccesos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfilAccesos()
    {
        return $this->hasMany(PerfilAcceso::className(), ['modulo_idmodulo' => 'idmodulo']);
    }

    /**
     * Gets query for [[Programas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProgramas()
    {
        return $this->hasMany(Programa::className(), ['modulo_idmodulo' => 'idmodulo']);
    }

    /**
     * Gets query for [[ModuloIdmodulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModuledescription()
    {
        return $this->sistemas->descripcion;
        
    }
}
