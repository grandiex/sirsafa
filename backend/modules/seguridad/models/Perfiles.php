<?php

namespace backend\modules\seguridad\models;

use Yii;

/**
 * This is the model class for table "perfiles".
 *
 * @property int $idperfiles Tabla para almacenar nombres de los posibles perfiles que existan en el sistema seleccionado - Pertenece al módulo de seguridad
 * @property string $descripcion
 * @property int $denominacion_iddenominacion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property PerfilAcceso[] $perfilAccesos
 * @property Denominacion $denominacionIddenominacion
 * @property PerfilesNotificacion[] $perfilesNotificacions
 * @property UsuarioInterno[] $usuarioInternos
 */
class Perfiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'denominacion_iddenominacion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['denominacion_iddenominacion', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['descripcion'], 'string', 'max' => 45],
            [['denominacion_iddenominacion'], 'exist', 'skipOnError' => true, 'targetClass' => Denominacion::className(), 'targetAttribute' => ['denominacion_iddenominacion' => 'iddenominacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idperfiles' => 'Idperfiles',
            'descripcion' => 'Descripción',
            'denominacion_iddenominacion' => 'Denominación',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[PerfilAccesos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfilAccesos()
    {
        return $this->hasMany(Perfilacceso::className(), ['perfiles_idperfiles' => 'idperfiles']);
    }

    /**
     * Gets query for [[DenominacionIddenominacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDenominacion()
    {
        return $this->hasOne(Denominacion::className(), ['iddenominacion' => 'denominacion_iddenominacion']);
    }

    /**
     * Gets query for [[PerfilesNotificacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfilesNotificacions()
    {
        return $this->hasMany(Perfilesnotificacion::className(), ['perfiles_idperfiles' => 'idperfiles']);
    }

    /**
     * Gets query for [[UsuarioInternos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioInternos()
    {
        return $this->hasMany(Usuariointerno::className(), ['perfiles_idperfiles' => 'idperfiles']);
    }

    /**
     * Gets query for [[UsuarioInternos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDenominacionperfil()
    {
        return $this->denominacion->descripcion;
    }
}
