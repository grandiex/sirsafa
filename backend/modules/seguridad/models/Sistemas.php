<?php

namespace backend\modules\seguridad\models;

use Yii;

/**
 * This is the model class for table "sistemas".
 *
 * @property int $idsistemas Tabla para almacenar nombres de los posibles sistemas que existan en el ecosistema - Pertenece al módulo de seguridad
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Modulo[] $modulos
 */
class Sistemas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sistemas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idsistemas' => 'Idsistemas',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Modulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulos()
    {
        return $this->hasMany(Modulo::className(), ['sistemas_idsistemas' => 'idsistemas']);
    }
}
