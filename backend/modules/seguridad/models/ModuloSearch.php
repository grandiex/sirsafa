<?php

namespace backend\modules\seguridad\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\seguridad\models\Modulo;

/**
 * ModuloSearch represents the model behind the search form of `backend\modules\seguridad\models\Modulo`.
 */
class ModuloSearch extends Modulo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmodulo', 'sistemas_idsistemas', 'user_id'], 'integer'],
            [['descripcion', 'imagen', 'ruta', 'información', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Modulo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idmodulo' => $this->idmodulo,
            'sistemas_idsistemas' => $this->sistemas_idsistemas,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'imagen', $this->imagen])
            ->andFilterWhere(['like', 'ruta', $this->ruta])
            ->andFilterWhere(['like', 'información', $this->información]);

        return $dataProvider;
    }
}
