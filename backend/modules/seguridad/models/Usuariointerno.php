<?php

namespace backend\modules\seguridad\models;

use Yii;

/**
 * This is the model class for table "usuario_interno".
 *
 * @property int $idusuario_interno Almacena los usuarios internos con su perfil correspondiente para cada usuario - Pertenece al módulo de Seguridad
 * @property int $perfiles_idperfiles
 * @property string $nombres
 * @property string $apellidos
 * @property string $nacionalidad
 * @property int $documento_identidad
 * @property string $cargo
 * @property string $telefono_movil
 * @property string $telefono_oficina
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Perfiles $perfilesIdperfiles
 */
class Usuariointerno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_interno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perfiles_idperfiles', 'nombres', 'apellidos', 'nacionalidad', 'documento_identidad', 'cargo', 'telefono_movil', 'telefono_oficina', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['perfiles_idperfiles', 'documento_identidad', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombres', 'apellidos', 'cargo', 'telefono_movil', 'telefono_oficina'], 'string', 'max' => 45],
            [['nacionalidad'], 'string', 'max' => 1],
            [['perfiles_idperfiles'], 'exist', 'skipOnError' => true, 'targetClass' => Perfiles::className(), 'targetAttribute' => ['perfiles_idperfiles' => 'idperfiles']],
        ];
    }
 
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusuario_interno' => 'Idusuario Interno',
            'perfiles_idperfiles' => 'Perfil',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'nacionalidad' => 'Nacionalidad',
            'documento_identidad' => 'Documento Identidad',
            'cargo' => 'Cargo',
            'telefono_movil' => 'Teléfono Móvil',
            'telefono_oficina' => 'Teléfono Oficina',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[PerfilesIdperfiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfiles()
    {
        return $this->hasOne(Perfiles::className(), ['idperfiles' => 'perfiles_idperfiles']);
    }
}
