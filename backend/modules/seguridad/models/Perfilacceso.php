<?php

namespace backend\modules\seguridad\models;

use Yii;

/**
 * This is the model class for table "perfil_acceso".
 *
 * @property int $idperfil_modulo Tabla para almacenar los diferentes accesos a los que tienen los perfiles de usuarios - Pertenece al módulo de seguridad
 * @property int $modulo_idmodulo
 * @property int $programa_idmodulo
 * @property int $perfiles_idperfiles
 * @property int $nuevo
 * @property int $editar
 * @property int $examinar
 * @property int $eliminar
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Programa $programaIdmodulo
 * @property Modulo $moduloIdmodulo
 * @property Perfiles $perfilesIdperfiles
 */
class Perfilacceso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perfil_acceso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modulo_idmodulo', 'programa_idmodulo', 'perfiles_idperfiles', 'nuevo', 'editar', 'examinar', 'eliminar', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['modulo_idmodulo', 'programa_idmodulo', 'perfiles_idperfiles', 'nuevo', 'editar', 'examinar', 'eliminar', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['programa_idmodulo'], 'exist', 'skipOnError' => true, 'targetClass' => Programa::className(), 'targetAttribute' => ['programa_idmodulo' => 'idmodulo']],
            [['modulo_idmodulo'], 'exist', 'skipOnError' => true, 'targetClass' => Modulo::className(), 'targetAttribute' => ['modulo_idmodulo' => 'idmodulo']],
            [['perfiles_idperfiles'], 'exist', 'skipOnError' => true, 'targetClass' => Perfiles::className(), 'targetAttribute' => ['perfiles_idperfiles' => 'idperfiles']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idperfil_modulo' => 'Idperfil Modulo',
            'modulo_idmodulo' => 'Módulo',
            'programa_idmodulo' => 'Programa',
            'perfiles_idperfiles' => 'Perfil',
            'nuevo' => 'Nuevo',
            'editar' => 'Editar',
            'examinar' => 'Examinar',
            'eliminar' => 'Eliminar',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[ProgramaIdmodulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrograma()
    {
        return $this->hasOne(Programa::className(), ['idmodulo' => 'programa_idmodulo']);
    }

    /**
     * Gets query for [[ModuloIdmodulo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulo()
    {
        return $this->hasOne(Modulo::className(), ['idmodulo' => 'modulo_idmodulo']);
    }

    /**
     * Gets query for [[PerfilesIdperfiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfiles()
    {
        return $this->hasOne(Perfiles::className(), ['idperfiles' => 'perfiles_idperfiles']);
    }
}
