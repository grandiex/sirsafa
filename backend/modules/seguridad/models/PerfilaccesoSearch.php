<?php

namespace backend\modules\seguridad\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\seguridad\models\Perfilacceso;

/**
 * PerfilaccesoSearch represents the model behind the search form of `backend\modules\seguridad\models\Perfilacceso`.
 */
class PerfilaccesoSearch extends Perfilacceso
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idperfil_modulo', 'modulo_idmodulo', 'programa_idmodulo', 'perfiles_idperfiles',  'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Perfilacceso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idperfil_modulo' => $this->idperfil_modulo,
            'modulo_idmodulo' => $this->modulo_idmodulo,
            'programa_idmodulo' => $this->programa_idmodulo,
            'perfiles_idperfiles' => $this->perfiles_idperfiles,
            //'nuevo' => $this->nuevo,
            //'editar' => $this->editar,
            //'examinar' => $this->examinar,
            //'eliminar' => $this->eliminar,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        return $dataProvider;
    }
}
