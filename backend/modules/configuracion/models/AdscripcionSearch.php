<?php

namespace backend\modules\configuracion\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\configuracion\models\Adscripcion;

/**
 * AdscripcionSearch represents the model behind the search form of `backend\modules\configuracion\models\Adscripcion`.
 */
class AdscripcionSearch extends Adscripcion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idadscripcion', 'user_id', 'tipo_idtipo'], 'integer'],
            [['descripcion', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adscripcion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idadscripcion' => $this->idadscripcion,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            //'tipo_idtipo' => $this->tipo_idtipo,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
