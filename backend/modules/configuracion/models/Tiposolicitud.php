<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "tipo_solicitud".
 *
 * @property int $idtipo_solicitud Se Almacenael tipo de solicitud a realizar el usuario externo - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property ClaseSolicitud[] $claseSolicituds
 * @property Solicitud[] $solicituds
 */
class Tiposolicitud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_solicitud';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtipo_solicitud' => 'Idtipo Solicitud',
            'descripcion' => 'Descripcion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[ClaseSolicituds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClaseSolicituds()
    {
        return $this->hasMany(ClaseSolicitud::className(), ['tipo_solicitud_idtipo_solicitud' => 'idtipo_solicitud']);
    }

    /**
     * Gets query for [[Solicituds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSolicituds()
    {
        return $this->hasMany(Solicitud::className(), ['tipo_solicitud_idtipo_solicitud' => 'idtipo_solicitud']);
    }
}
