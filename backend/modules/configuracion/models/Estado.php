<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "estado".
 *
 * @property int $idestado Se Almacena el estado (division politico territorial) - Pertenece al módulo de Administración del sistema
 * @property string|null $codigo
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Aprovechamiento[] $aprovechamientos
 * @property EstadoRegionHidrografica[] $estadoRegionHidrograficas
 * @property Municipio[] $municipios
 * @property PersonaJuridica[] $personaJuridicas
 * @property PersonaNatural[] $personaNaturals
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion','codigo', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestado' => 'Idestado',
            'codigo' => 'Código',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Aprovechamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientos()
    {
        return $this->hasMany(Aprovechamiento::className(), ['estado_idestado' => 'idestado']);
    }

    /**
     * Gets query for [[EstadoRegionHidrograficas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoRegionHidrograficas()
    {
        return $this->hasMany(EstadoRegionHidrografica::className(), ['estado_idestado' => 'idestado']);
    }

    /**
     * Gets query for [[Municipios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipios()
    {
        return $this->hasMany(Municipio::className(), ['estado_idestado' => 'idestado']);
    }

    /**
     * Gets query for [[PersonaJuridicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaJuridicas()
    {
        return $this->hasMany(PersonaJuridica::className(), ['estado_idestado' => 'idestado']);
    }

    /**
     * Gets query for [[PersonaNaturals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaNaturals()
    {
        return $this->hasMany(PersonaNatural::className(), ['estado_idestado' => 'idestado']);
    }
}
