<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "estado_civil".
 *
 * @property int $idestado_civil Tabla para almacenar los estados civiles  - Pertenece al módulo de Administracion del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property PersonaNatural[] $personaNaturals
 */
class Estadocivil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_civil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestado_civil' => 'Idestado Civil',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[PersonaNaturals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaNaturals()
    {
        return $this->hasMany(PersonaNatural::className(), ['estado_civil_idestado_civil' => 'idestado_civil']);
    }
}
