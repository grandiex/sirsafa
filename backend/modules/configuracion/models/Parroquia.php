<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "parroquia".
 *
 * @property int $idparroquia Se Almacena la parroquia (division politico territorial) - Pertenece al módulo de Administración del sistema
 * @property string $codigo
 * @property int $municipio_idmunicipio
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Aprovechamiento[] $aprovechamientos
 * @property Municipio $municipioIdmunicipio
 * @property PersonaJuridica[] $personaJuridicas
 * @property PersonaNatural[] $personaNaturals
 */
class Parroquia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parroquia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'municipio_idmunicipio', 'descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['municipio_idmunicipio', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['municipio_idmunicipio'], 'exist', 'skipOnError' => true, 'targetClass' => Municipio::className(), 'targetAttribute' => ['municipio_idmunicipio' => 'idmunicipio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idparroquia' => 'Idparroquia',
            'codigo' => 'Código',
            'municipio_idmunicipio' => 'Municipio',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Aprovechamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientos()
    {
        return $this->hasMany(Aprovechamiento::className(), ['parroquia_idparroquia' => 'idparroquia']);
    }

    /**
     * Gets query for [[MunicipioIdmunicipio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipio()
    {
        return $this->hasOne(Municipio::className(), ['idmunicipio' => 'municipio_idmunicipio']);
    }

    /**
     * Gets query for [[PersonaJuridicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaJuridicas()
    {
        return $this->hasMany(PersonaJuridica::className(), ['parroquia_idparroquia' => 'idparroquia']);
    }

    /**
     * Gets query for [[PersonaNaturals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaNaturals()
    {
        return $this->hasMany(PersonaNatural::className(), ['parroquia_idparroquia' => 'idparroquia']);
    }
}
