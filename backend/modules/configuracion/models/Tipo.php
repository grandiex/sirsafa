<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "tipo".
 *
 * @property int $idtipo Se Almacenael tipo de dominio de la persona juridica - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property int $dominio_iddominio
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Adscripcion[] $adscripcions
 * @property Dominio $dominioIddominio
 */
class Tipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'dominio_iddominio', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['dominio_iddominio', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['descripcion'], 'string', 'max' => 45],
            [['dominio_iddominio'], 'exist', 'skipOnError' => true, 'targetClass' => Dominio::className(), 'targetAttribute' => ['dominio_iddominio' => 'iddominio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtipo' => 'Idtipo',
            'descripcion' => 'Descripción',
            'dominio_iddominio' => 'Dominio',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Adscripcions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdscripcions()
    {
        return $this->hasMany(Adscripcion::className(), ['tipo_idtipo' => 'idtipo']);
    }

    /**
     * Gets query for [[DominioIddominio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDominio()
    {
        return $this->hasOne(Dominio::className(), ['iddominio' => 'dominio_iddominio']);
    }
}
