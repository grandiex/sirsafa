<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "clase_solicitud".
 *
 * @property int $idclase_solicitud Se Almacenaclase de solicitud a realizar el usuario externo - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property int $tipo_solicitud_idtipo_solicitud
 * @property int $activar_fecha
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property TipoSolicitud $tipoSolicitudIdtipoSolicitud
 * @property Solicitud[] $solicituds
 */
class Clasesolicitud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clase_solicitud';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'tipo_solicitud_idtipo_solicitud', 'activar_fecha', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['tipo_solicitud_idtipo_solicitud', 'activar_fecha', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['descripcion'], 'string', 'max' => 45],
            [['tipo_solicitud_idtipo_solicitud'], 'exist', 'skipOnError' => true, 'targetClass' => Tiposolicitud::className(), 'targetAttribute' => ['tipo_solicitud_idtipo_solicitud' => 'idtipo_solicitud']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idclase_solicitud' => 'Idclase Solicitud',
            'descripcion' => 'Descripción',
            'tipo_solicitud_idtipo_solicitud' => 'Tipo de Solicitud',
            'activar_fecha' => 'Activar Fecha',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[TipoSolicitudIdtipoSolicitud]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTiposolicitud()
    {
        return $this->hasOne(Tiposolicitud::className(), ['idtipo_solicitud' => 'tipo_solicitud_idtipo_solicitud']);
    }

    /**
     * Gets query for [[Solicituds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSolicituds()
    {
        return $this->hasMany(Solicitud::className(), ['clase_solicitud_idclase_solicitud' => 'idclase_solicitud']);
    }
}
