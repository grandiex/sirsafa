<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "tipo_usos".
 *
 * @property int $idtipo_usos Se Almacena el tipo de uso con fines de aprovechamiento  - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 * @property int $usos_idusos
 * @property int $id_padre
 *
 * @property AprovechamientoUsos[] $aprovechamientoUsos
 */
class Tipousos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_usos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id', 'usos_idusos', 'id_padre'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'usos_idusos', 'id_padre'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtipo_usos' => 'Idtipo Usos',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
            'usos_idusos' => 'Usos',
            'id_padre' => 'Id Padre',
        ];
    }

    /**
     * Gets query for [[AprovechamientoUsos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientoUsos()
    {
        return $this->hasMany(AprovechamientoUsos::className(), ['tipo_usos_idtipo_usos' => 'idtipo_usos']);
    }
    public function getUsos()
    {
        return $this->hasOne(Usos::className(), ['idtipo_usos' => 'usos_idusos']);
    }

}
