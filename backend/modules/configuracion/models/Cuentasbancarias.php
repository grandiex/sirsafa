<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "cuentas_bancarias".
 *
 * @property int $idcuentas_bancarias Tabla para almacenar nombres de los posibles sistemas que existan en el ecosistema - Pertenece al módulo de seguridad
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 * @property string $nro_cuenta
 * @property string $estatus
 * @property int $bancos_idbancos
 *
 * @property ArchivoTxt[] $archivoTxts
 * @property Bancos $bancosIdbancos
 */
class Cuentasbancarias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuentas_bancarias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id', 'nro_cuenta', 'estatus', 'bancos_idbancos'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'bancos_idbancos'], 'integer'],
            [['descripcion', 'nro_cuenta', 'estatus'], 'string', 'max' => 45],
            [['bancos_idbancos'], 'exist', 'skipOnError' => true, 'targetClass' => Bancos::className(), 'targetAttribute' => ['bancos_idbancos' => 'idbancos']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcuentas_bancarias' => 'Cuentas Bancarias',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
            'nro_cuenta' => 'Nro. de Cuenta',
            'estatus' => 'Estatus',
            'bancos_idbancos' => 'Bancos',
        ];
    }

    /**
     * Gets query for [[ArchivoTxts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArchivoTxts()
    {
        return $this->hasMany(ArchivoTxt::className(), ['cuentas_bancarias_idcuentas_bancarias' => 'idcuentas_bancarias']);
    }

    /**
     * Gets query for [[BancosIdbancos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBancos()
    {
        return $this->hasOne(Bancos::className(), ['idbancos' => 'bancos_idbancos']);
    }
}
