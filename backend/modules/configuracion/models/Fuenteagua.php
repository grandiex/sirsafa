<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "fuente_agua".
 *
 * @property int $idfuente_agua Se Almacena el dominio de la persona juridica - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Aprovechamiento[] $aprovechamientos
 */
class Fuenteagua extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fuente_agua';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfuente_agua' => 'Idfuente Agua',
            'descripcion' => 'Descripcion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Aprovechamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientos()
    {
        return $this->hasMany(Aprovechamiento::className(), ['fuente_agua_idfuente_agua' => 'idfuente_agua']);
    }
}
