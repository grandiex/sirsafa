<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "dominio".
 *
 * @property int $iddominio Se Almacena el dominio de la persona juridica - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 */
class Dominio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dominio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddominio' => 'Iddominio',
            'descripcion' => 'Descripcion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }
}
