<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "bancos".
 *
 * @property int $idbancos Tabla para almacenar nombres de los posibles sistemas que existan en el ecosistema - Pertenece al módulo de seguridad
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property CuentasBancarias[] $cuentasBancarias
 */
class Bancos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bancos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idbancos' => 'Idbancos',
            'descripcion' => 'Descripcion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[CuentasBancarias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuentasBancarias()
    {
        return $this->hasMany(CuentasBancarias::className(), ['bancos_idbancos' => 'idbancos']);
    }
}
