<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "municipio".
 *
 * @property int $idmunicipio Se Almacena el minucipio (division politico territorial) - Pertenece al módulo de Administración del sistema
 * @property string|null $codigo
 * @property int $estado_idestado
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property Aprovechamiento[] $aprovechamientos
 * @property Estado $estadoIdestado
 * @property Parroquia[] $parroquias
 * @property PersonaJuridica[] $personaJuridicas
 * @property PersonaNatural[] $personaNaturals
 */
class Municipio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'municipio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado_idestado', 'codigo','descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['estado_idestado', 'user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['codigo', 'descripcion'], 'string', 'max' => 45],
            [['estado_idestado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['estado_idestado' => 'idestado']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmunicipio' => 'Idmunicipio',
            'codigo' => 'Código',
            'estado_idestado' => 'Estado',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[Aprovechamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientos()
    {
        return $this->hasMany(Aprovechamiento::className(), ['municipio_idmunicipio' => 'idmunicipio']);
    }

    /**
     * Gets query for [[EstadoIdestado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['idestado' => 'estado_idestado']);
    }

    /**
     * Gets query for [[Parroquias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParroquias()
    {
        return $this->hasMany(Parroquia::className(), ['municipio_idmunicipio' => 'idmunicipio']);
    }

    /**
     * Gets query for [[PersonaJuridicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaJuridicas()
    {
        return $this->hasMany(PersonaJuridica::className(), ['municipio_idmunicipio' => 'idmunicipio']);
    }

    /**
     * Gets query for [[PersonaNaturals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaNaturals()
    {
        return $this->hasMany(PersonaNatural::className(), ['municipio_idmunicipio' => 'idmunicipio']);
    }

    /**
     * Gets query for [[PersonaNaturals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadodescription()
    {
        return $this->estado->descripcion;
    }
}
