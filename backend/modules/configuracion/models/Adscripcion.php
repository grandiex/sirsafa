<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "adscripcion".
 *
 * @property int $idadscripcion Se Almacenael tipo de adscripcion de la persona juridica - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 * @property int $tipo_idtipo
 *
 * @property Tipo $tipoIdtipo
 * @property AdscripcionPersonaJuridica[] $adscripcionPersonaJuridicas
 */
class Adscripcion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adscripcion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id', 'tipo_idtipo'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'tipo_idtipo'], 'integer'],
            [['descripcion'], 'string', 'max' => 100],
            [['tipo_idtipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_idtipo' => 'idtipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idadscripcion' => 'Idadscripcion',
            'descripcion' => 'Descripción',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
            'tipo_idtipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[TipoIdtipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['idtipo' => 'tipo_idtipo']);
    }

    /**
     * Gets query for [[AdscripcionPersonaJuridicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdscripcionPersonaJuridicas()
    {
        return $this->hasMany(AdscripcionPersonaJuridica::className(), ['adscripcion_idadscripcion' => 'idadscripcion']);
    }
}
