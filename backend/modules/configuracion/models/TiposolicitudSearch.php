<?php

namespace backend\modules\configuracion\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\configuracion\models\Tiposolicitud;

/**
 * TiposolicitudSearch represents the model behind the search form of `backend\modules\configuracion\models\Tiposolicitud`.
 */
class TiposolicitudSearch extends Tiposolicitud
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtipo_solicitud', 'user_id'], 'integer'],
            [['descripcion', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tiposolicitud::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idtipo_solicitud' => $this->idtipo_solicitud,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
