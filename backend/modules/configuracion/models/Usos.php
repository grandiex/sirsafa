<?php

namespace backend\modules\configuracion\models;

use Yii;

/**
 * This is the model class for table "usos".
 *
 * @property int $idusos Se Almacena el uso con fines de aprovechamiento  - Pertenece al módulo de Administración del sistema
 * @property string $descripcion
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 *
 * @property AprovechamientoUsos[] $aprovechamientoUsos
 */
class Usos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'created_at', 'updated_at', 'user_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusos' => 'Idusos',
            'descripcion' => 'Descripcion',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'user_id' => 'Usuario',
        ];
    }

    /**
     * Gets query for [[AprovechamientoUsos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprovechamientoUsos()
    {
        return $this->hasMany(AprovechamientoUsos::className(), ['usos_idusos' => 'idusos']);
    }
}
