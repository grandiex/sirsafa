<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Actividadeconomica */

$this->title = Yii::t('app', 'Actualizar Actividad Económica: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Actividad Económicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idactividad_economica, 'url' => ['view', 'id' => $model->idactividad_economica]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="actividadeconomica-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
