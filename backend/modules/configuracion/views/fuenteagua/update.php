<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Fuenteagua */

$this->title = Yii::t('app', 'Actualizar Fuente de Agua: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Fuente de Aguas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idfuente_agua, 'url' => ['view', 'id' => $model->idfuente_agua]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="fuenteagua-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
