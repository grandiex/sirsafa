<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Adscripcion */

$this->title = Yii::t('app', 'Nuevo Adscripto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Adscripción'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adscripcion-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
