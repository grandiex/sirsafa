<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Adscripcion */

$this->title = Yii::t('app', 'Actualizar Adscripción: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Adscripción'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idadscripcion, 'url' => ['view', 'id' => $model->idadscripcion]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="adscripcion-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
