<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Sexo */

$this->title = $model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sexos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sexo-view">

    <h1  class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idsexo, 'idprogram'=>$idprogram,], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idsexo, 'idprogram'=>$idprogram,], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idsexo',
            'descripcion',
            'created_at',
            'updated_at',
            //'user_id',
        ],
    ]) ?>

</div>
