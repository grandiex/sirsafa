<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Sexo */

$this->title = Yii::t('app', 'Actualizar Sexo: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sexos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsexo, 'url' => ['view', 'id' => $model->idsexo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="sexo-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
