<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Sexo */

$this->title = Yii::t('app', 'Nuevo Sexo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sexos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sexo-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
