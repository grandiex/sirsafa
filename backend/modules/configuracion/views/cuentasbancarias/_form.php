<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\configuracion\models\Bancos;


/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Cuentasbancarias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuentasbancarias-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">
    
    <div class="col-md-6 col-sm-6">
            
            <?= $form->field($model, 'bancos_idbancos')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Bancos::find()->orderBy('descripcion ASC')->all(), 'idbancos','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],]); ?>
    
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'nro_cuenta')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6 col-sm-6">
        <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
            'data' => ["ACTIVA"=>"ACTIVA", "INACTIVA"=>"INACTIVA"],
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],]); ?>
    </div>

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
