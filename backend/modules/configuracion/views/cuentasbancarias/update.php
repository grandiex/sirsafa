<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Cuentasbancarias */

$this->title = Yii::t('app', 'Actualizar Cuentas bancarias: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentas bancarias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcuentas_bancarias, 'url' => ['view', 'id' => $model->idcuentas_bancarias]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="cuentasbancarias-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
