<?php


use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\growl\Growl;
use backend\modules\configuracion\models\Bancos;

use backend\modules\seguridad\models\Perfilacceso;

$idprogram=$_GET["idprogram"];

$programa = Perfilacceso::find()->where("idperfil_modulo=$idprogram")->one();
$template="";


if ($programa->examinar==1) {
    $template.= '{view} ' ;
}
if ($programa->editar==1) {
    $template.= '{update} ' ;
}
if ($programa->eliminar==1) {
    $template.= '{delete} ' ;
}


if ($programa->nuevo==1) {
    $newregister= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'idprogram'=>$idprogram,], ['class' => 'btn btn-success', 'title' => 'Nuevo Registro']) ;
}else{
    $newregister="";
}



/* @var $this yii\web\View */
/* @var $searchModel backend\modules\configuracion\models\CuentasbancariasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cuentas bancarias');
$this->params['breadcrumbs'][] = $this->title;

if ( Yii::$app->session->getFlash('success') ) {
    $type = Growl::TYPE_SUCCESS;
    $title = 'Guardar Registro';
    $icon = 'glyphicon glyphicon-ok-sign';
    $body = Yii::$app->session->getFlash('success');
} elseif ( Yii::$app->session->getFlash('error') ) {
    $type = Growl::TYPE_DANGER;
    $title = 'Error al Guardar Registro';
    $icon = 'glyphicon glyphicon-remove-sign';
    $body = Yii::$app->session->getFlash('error');
} elseif ( Yii::$app->session->getFlash('warning') ) {
    $type = Growl::TYPE_WARNING;
    $title = 'Registro Eliminado';
    $icon = 'glyphicon glyphicon-exclamation-sign';
    $body = Yii::$app->session->getFlash('warning');
}

$flashMessages = Yii::$app->session->getAllFlashes();
if ($flashMessages) {
        echo Growl::widget([
            'type' => $type,
            'title' => $title,
            'icon' => $icon,
            'body' => $body,
            'showSeparator' => true,
            'delay' => 0,
            'pluginOptions' => [
                'showProgressbar' => true,
                'placement' => [
                    'from' => 'top',
                    'align' => 'center',
                ],
                'timer' => 1000,
            ]
        ]);
}
?>
<div class="cuentasbancarias-index">

<?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'idsistemas',
            //'bancos_idbancos',
            [
                //'class' => 'kartik\grid\BooleanColumn',
                'attribute' => 'bancos_idbancos', 
                'value' => 'bancos.descripcion', 
                //'label'=>'Estatus',
                //'vAlign' => 'middle',
                
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Bancos::find()->orderBy('descripcion ASC')->all(), 'idbancos','descripcion' ),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Opciones'],
                'format' => 'raw'
                
            ],
            'descripcion',
            'nro_cuenta',
            'estatus',
            //'created_at',
            
            
        ['class' => 'kartik\grid\ActionColumn', 

            'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => $template,
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },

                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Eliminar'),
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                    ]);
                },


                ],

                'urlCreator' => function ($action, $model, $key, $index) use ($idprogram){
                /*if ($action === 'update') {
                    $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                    return $url;
                }*/
                if ($action === 'view') {
                    $url =['/configuracion/cuentasbancarias/view?idprogram='.$idprogram.'&id='.$model->idcuentas_bancarias];
                    return $url;
                }
                if ($action === 'update') {
                    $url =['/configuracion/cuentasbancarias/update?idprogram='.$idprogram.'&id='.$model->idcuentas_bancarias];
                    return $url;
                }
                if ($action === 'delete') {
                    $url =['/configuracion/cuentasbancarias/delete?idprogram='.$idprogram.'&id='.$model->idcuentas_bancarias];
                    return $url;
                }
                  

                }

        ],

            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                $newregister . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true, 
        
        'panel' => [
            'heading'=>'<h1 class="h2-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h2>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>


</div>
