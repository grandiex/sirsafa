<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Cuentasbancarias */

$this->title = Yii::t('app', 'Nuevas Cuentas bancarias');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuentas bancarias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuentasbancarias-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
