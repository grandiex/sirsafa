<?php


use kartik\grid\GridView;
use kartik\helpers\Html;
use kartik\select2\Select2;
use kartik\growl\Growl;

use backend\modules\seguridad\models\Perfilacceso;

$idprogram=$_GET["idprogram"];

$programa = Perfilacceso::find()->where("idperfil_modulo=$idprogram")->one();
$template="";


if ($programa->examinar==1) {
    $template.= '{view} ' ;
}
if ($programa->editar==1) {
    $template.= '{update} ' ;
}
if ($programa->eliminar==1) {
    $template.= '{delete} ' ;
}


if ($programa->nuevo==1) {
    $newregister= Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'idprogram'=>$idprogram,], ['class' => 'btn btn-success', 'title' => 'Nuevo Registro']) ;
}else{
    $newregister="";
}



/* @var $this yii\web\View */
/* @var $searchModel backend\modules\configuracion\models\BancosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bancos');
$this->params['breadcrumbs'][] = $this->title;

if ( Yii::$app->session->getFlash('success') ) {
    $type = Growl::TYPE_SUCCESS;
    $title = 'Guardar Registro';
    $icon = 'glyphicon glyphicon-ok-sign';
    $body = Yii::$app->session->getFlash('success');
} elseif ( Yii::$app->session->getFlash('error') ) {
    $type = Growl::TYPE_DANGER;
    $title = 'Error al Guardar Registro';
    $icon = 'glyphicon glyphicon-remove-sign';
    $body = Yii::$app->session->getFlash('error');
} elseif ( Yii::$app->session->getFlash('warning') ) {
    $type = Growl::TYPE_WARNING;
    $title = 'Registro Eliminado';
    $icon = 'glyphicon glyphicon-exclamation-sign';
    $body = Yii::$app->session->getFlash('warning');
}

$flashMessages = Yii::$app->session->getAllFlashes();
if ($flashMessages) {
        echo Growl::widget([
            'type' => $type,
            'title' => $title,
            'icon' => $icon,
            'body' => $body,
            'showSeparator' => true,
            'delay' => 0,
            'pluginOptions' => [
                'showProgressbar' => true,
                'placement' => [
                    'from' => 'top',
                    'align' => 'center',
                ],
                'timer' => 1000,
            ]
        ]);
}
?>
<div class="bancos-index">


<?php 
    

    $gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],

    
            //'idsistemas',
            'descripcion',
            //'created_at',
            
            
        ['class' => 'kartik\grid\ActionColumn', 

            'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => $template,
                'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },

                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Eliminar'),
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                    ]);
                },


                ],

                'urlCreator' => function ($action, $model, $key, $index) use ($idprogram){
                /*if ($action === 'update') {
                    $url ='index.php?r=cambiosdivisas/operaciones/view&id='.$model->idoperacion;
                    return $url;
                }*/
                if ($action === 'view') {
                    $url =['/configuracion/bancos/view?idprogram='.$idprogram.'&id='.$model->idbancos];
                    return $url;
                }
                if ($action === 'update') {
                    $url =['/configuracion/bancos/update?idprogram='.$idprogram.'&id='.$model->idbancos];
                    return $url;
                }
                if ($action === 'delete') {
                    $url =['/configuracion/bancos/delete?idprogram='.$idprogram.'&id='.$model->idbancos];
                    return $url;
                }
                  

                }

        ],

            //['class' => 'kartik\grid\CheckboxColumn']
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        
        'resizableColumns'=>true,
        
        'toolbar' =>  [
            ['content'=>
                $newregister . ' '.
                    
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Refrescar Consulta'])
            ],
            '{export}',
            '{toggleData}'
        ],

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true, 
        
        'panel' => [
            'heading'=>'<h1 class="h2-head"><i class="glyphicon glyphicon-cog"> </i> '.Html::encode($this->title).'</h2>',
            'type' => GridView::TYPE_PRIMARY,
            'footer'=>true
        ],
    
    ]);

    ?>


</div>
