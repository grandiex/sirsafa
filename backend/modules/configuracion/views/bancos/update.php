<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Bancos */

$this->title = Yii::t('app', 'Actualizar Bancos: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bancos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idbancos, 'url' => ['view', 'id' => $model->idbancos]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="bancos-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
