<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Parroquia */

$this->title = Yii::t('app', 'Actualizar Parroquia: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parroquias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idparroquia, 'url' => ['view', 'id' => $model->idparroquia]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="parroquia-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
