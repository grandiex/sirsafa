<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Tiposolicitud */

$this->title = Yii::t('app', 'Actualizar Tipo de Solicitud: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo de Solicituds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtipo_solicitud, 'url' => ['view', 'id' => $model->idtipo_solicitud]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="tiposolicitud-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
