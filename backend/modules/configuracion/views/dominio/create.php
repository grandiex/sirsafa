<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Dominio */

$this->title = Yii::t('app', 'Nuevo Dominio');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dominio'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dominio-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
