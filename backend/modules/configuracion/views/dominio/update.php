<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Dominio */

$this->title = Yii::t('app', 'Actualizar Dominio: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dominio'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddominio, 'url' => ['view', 'id' => $model->iddominio]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="dominio-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
