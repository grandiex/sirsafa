<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Usos */

$this->title = Yii::t('app', 'Actualizar Usos: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idusos, 'url' => ['view', 'id' => $model->idusos]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="usos-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
