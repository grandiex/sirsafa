<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Clasesolicitud */

$this->title = Yii::t('app', 'Actualizar Clase de Solicitud: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clases de Solicitudes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idclase_solicitud, 'url' => ['view', 'id' => $model->idclase_solicitud]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="clasesolicitud-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
