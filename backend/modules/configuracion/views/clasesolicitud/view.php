<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Clasesolicitud */

$this->title = Yii::t('app', 'Clase de Solicitud: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clase de Solicitudes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clasesolicitud-view">

    <h1  class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idclase_solicitud, 'idprogram'=>$idprogram,], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idclase_solicitud, 'idprogram'=>$idprogram,], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idclase_solicitud',
            [
                'attribute' => 'tipo_solicitud_idtipo_solicitud', 
                'value' =>$model->tiposolicitud->descripcion, 
            ],
            'descripcion',
            
            [
                'attribute' => 'activar_fecha',
                'format'=>'raw',
                'value'=> function ($model){

                    if ($model->activar_fecha==0) {
                        return '<span class="tex text-danger glyphicon glyphicon-remove-circle "> </span>'; 
                    }else{

                        return '<span class="tex text-success glyphicon glyphicon-ok-circle "> </span>';
                    }
                }
            ],
            'created_at',
            'updated_at',
            //'user_id',
        ],
    ]) ?>

</div>
