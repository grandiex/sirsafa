<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;

use backend\modules\configuracion\models\Tiposolicitud;
/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Clasesolicitud */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clasesolicitud-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

        <div class="col-md-4 col-sm-4">
            
            <?= $form->field($model, 'tipo_solicitud_idtipo_solicitud')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tiposolicitud::find()->orderBy('descripcion ASC')->all(), 'idtipo_solicitud','descripcion' ),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],]); ?>
        </div>

        <div class="col-md-4 col-sm-4">
            <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
        </div>
                               
        <div class="col-md-4 col-sm-4">
            
            <?= $form->field($model, 'activar_fecha')->widget(SwitchInput::classname(), []) ?>
        </div>
        

    

        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
