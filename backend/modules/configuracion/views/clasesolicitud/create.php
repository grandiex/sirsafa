<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Clasesolicitud */

$this->title = Yii::t('app', 'Nueva Clase de Solicitud');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clases de Solicitudes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clasesolicitud-create">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div> 
