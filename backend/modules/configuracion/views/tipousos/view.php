<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Tipousos */

$this->title = $model->descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo de Usos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tipousos-view">

    <h1  class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>
    <br>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idtipo_usos, 'idprogram'=>$idprogram,], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idtipo_usos, 'idprogram'=>$idprogram,], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idtipo_usos',
            'descripcion',
            'created_at',
            'updated_at',
            'user_id',
            'usos_idusos',
            'id_padre',
        ],
    ]) ?>

</div>
