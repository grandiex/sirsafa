<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Tipousos */

$this->title = Yii::t('app', 'Actualizar Tipo de Usos: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo de Usos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtipo_usos, 'url' => ['view', 'id' => $model->idtipo_usos]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="tipousos-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
