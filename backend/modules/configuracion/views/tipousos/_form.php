<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use backend\modules\configuracion\models\Usos;


/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Tipousos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipousos-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">
    <div class="col-md-6 col-sm-6">
            
            <?= $form->field($model, 'usos_idusos')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Usos::find()->orderBy('descripcion ASC')->all(), 'idusos','descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar una Opción ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],]); ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>
        </div>


        <div class="form-group col-md-12 col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
           <?=  Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>

</div>
