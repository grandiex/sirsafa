<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\configuracion\models\Estadocivil */

$this->title = Yii::t('app', 'Actualizar Estado Civil: {name}', [
    'name' => $model->descripcion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estado Civiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idestado_civil, 'url' => ['view', 'id' => $model->idestado_civil]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="estadocivil-update">

    <h1 class=" text-center bg bg-gold"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'idprogram'=>$idprogram,
    ]) ?>

</div>
