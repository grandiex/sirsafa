<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'sirsafa-backend',
    'name' => 'SISTEMA DE REGISTRO Y SEGUIMIENTO A LOS APROVECHAMIENTOS EN FUENTES DE AGUA',
    'language'=>'es',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [        
        'user' => [
            // following line will restrict access to profile, recovery, registration and settings controllers from backend
            'as backend' => 'dektrium\user\filters\BackendFilter',
        ],
        
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => [
                    // 'class' => 'yii\i18n\PhpMessageSource',
                    // 'basePath' => '@kvgrid/messages',
                    // 'forceTranslation' => true]
        ],
        /*Módulos SIRSAFA*/
        'seguridad' => [
            'class' => 'backend\modules\seguridad\Module',
        ],
        'configuracion' => [
            'class' => 'backend\modules\configuracion\Module',
        ],
        'operaciones' => [
            'class' => 'backend\modules\operaciones\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        /*'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],*/
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'i18n' => [

            'translations' => [

                'app' => [

                    'class' => 'yii\i18n\PhpMessageSource',

                    'basePath' => '@common/messages',

                    'sourceLanguage' => 'en-US',

                ],
                 'yii' => [

                    'class' => 'yii\i18n\PhpMessageSource',

                    'basePath' => '@common/messages',

                    'sourceLanguage' => 'en-US',

                ],                

             ],

        ],
        
    ],
    /*'modules' => [

    ],*/
    'params' => $params,
];
