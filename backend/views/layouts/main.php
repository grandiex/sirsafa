<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

//use backend\modules\seguridad\models\Sistemas;
//use backend\modules\seguridad\models\Modulo;
//use backend\modules\seguridad\models\Programa;
use backend\modules\seguridad\models\Perfilacceso;

use backend\modules\seguridad\models\Usuariointerno;

AppAsset::register($this);

//$Sistema = Sistemas::find()->one();


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php echo '<div class="border-gold">'.Html::img('@web/images/cintillo.png', ['alt' => 'SIRSAFA','title' => 'SIRSAFA', 'width'=>'70%' ]).'</div>'; ?>


<div class="wrap">

    <?php


    $menuItems = [];

   
    ?>
    <?php
    if (!Yii::$app->user->isGuest) {
        $usuarioactivo=Usuariointerno::find()->where(['user_id'=>Yii::$app->user->identity->getId()])->one();
    
            NavBar::begin([
                'brandLabel' => 'S.I.R.S.A.F.A',//Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            
/*
                $Modulos = Modulo::find()->where("sistemas_idsistemas=$Sistema->idsistemas")->orderBy('descripcion')->all();
                $count=1;
                $submenu=[];
                foreach($Modulos as $modulo) {

                    $Programas = Programa::find()->where("modulo_idmodulo=$modulo->idmodulo")->orderBy('descripcion')->all();
                     
                        foreach($Programas as $programa) {
            
                            $submenu[]=['label' => $programa->descripcion, 'url' => [$programa->ruta]];
                        }

                    $menuItems[0] = ['label' => '<span class="glyphicon glyphicon-home"> </span> Inicio ', 'url' => ['/']];

                    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-'.$modulo->imagen.'"> </span> '.$modulo->descripcion.' ', 'url' => ['#'],

                           'items' => $submenu,
                    ];

                     
                }
*/
                if ($usuarioactivo) {
                   
                    $Menus = Perfilacceso::find()->where("perfiles_idperfiles=$usuarioactivo->perfiles_idperfiles")->orderBy('modulo_idmodulo')->groupby('modulo_idmodulo')->all();
                    $count=1;
                    
                    foreach($Menus as $menu) {
                    $submenu=[];
                       
                        $Submenus = Perfilacceso::find()->where("perfiles_idperfiles=$usuarioactivo->perfiles_idperfiles")
                        ->andFilterWhere(['modulo_idmodulo'=>$menu->modulo_idmodulo])
                        ->orderBy('programa_idmodulo')->groupby('programa_idmodulo')->all();
                         
                            foreach($Submenus as $submenuaccess) {
                
                                $submenu[]=['label' => $submenuaccess->programa->descripcion, 'url' => [$submenuaccess->programa->ruta.'?idprogram='.$submenuaccess->idperfil_modulo]];
                            }

                        // $menuItems[0] = ['label' => '<span class="glyphicon glyphicon-home"> </span> Inicio ', 'url' => ['/']];

                        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-'.$menu->modulo->imagen.'"> </span> '.$menu->modulo->descripcion.' ', 'url' => ['#'],

                               'items' => $submenu,
                        ];

                         
                    }

                }




            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Login', 'url' => ['/site/login']];
            } else {

                $menuItems[] = ['label' => '<span class="glyphicon glyphicon-user"> </span> Perfil de Usuario', 'url' => ['#'],
                        'items' => [
                            
                            ['label' => 'Perfil', 'url' => ['/seguridad/usuariointerno/perfil']],
                            ['label' => 'Cuenta', 'url' => ['/user/settings/account']],

                            ['label' => '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        '<span class="glyphicon glyphicon-log-out"> </span> Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout', 'style'=>'color:#333!important']
                    )
                    . Html::endForm()
                    . '</li>', 'url' => ['/user/settings/account']],

                        ],
                    ];

                // $menuItems[] = '<li>'
                //     . Html::beginForm(['/site/logout'], 'post')
                //     . Html::submitButton(
                //         '<span class="glyphicon glyphicon-user"> </span> Logout (' . Yii::$app->user->identity->username . ')',
                //         ['class' => 'btn btn-link logout']
                //     )
                //     . Html::endForm()
                //     . '</li>';
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'encodeLabels' => false,
                'items' => $menuItems,
            ]);
            NavBar::end();
    }
    ?>

    <div class="container">
        <!-- <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?> -->
        <br>
        <!-- <?php //Alert::widget() ?> -->
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; SIRSAFA <?= date('Y') ?></p>

        <p class="pull-right">Desarrollado  por  <?= Html::a(Html::decode('Grandiex', ['alt' => 'Grandiex.com','title' => 'Grandiex.com', 'width'=>'50%' ]),'https://grandiex.com/') ?></p>


        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
