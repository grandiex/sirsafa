<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper; 

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
    <div class="col-md-12 col-sm-12 bg bg-form">

        <?php foreach ($generator->getColumnNames() as $attribute) {
            if (in_array($attribute, $safeAttributes)) {
                echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
            }
        } ?>
        <div class="form-group col-md-12 col-sm-12">
            <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Save') ?>, ['class' => 'btn btn-success']) ?>
           <?= "<?= " ?> Html::a('Cancelar', ['index', 'idprogram'=>$idprogram,], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Cancelar']) ?>
        </div>
    </div> 
    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
